from tabulate import tabulate

class Amigos:
	def amigo(self):
		while True:
			dict_amigo = {}
			amigo = input("[A]migo o [S]alir: ").lower()
			if amigo == 'a':
				self.nombre = input("Nombre: ")
				self.edad = int(input("Edad: "))
				dict_amigo['nombre'] = self.nombre
				dict_amigo['edad'] = self.edad
				self.arrayAmigo.append(dict_amigo)
			elif amigo == 's':
				self.guardarAmigo(self.arrayAmigo)
				print("Adios!!!")
				break
			else:
				return self.amigo()

	def guardarAmigo(self, amigo):
		
		try:
			table = []
			archivo = open("amigos_t.txt", 'w')
			title = "---------- Lista de Amigos -----------\n"
			archivo.write(title)
			headers = ["Nro", "Nombre", "Edad"]
			for id, persona in enumerate(amigo):
				a = [id +1, persona['nombre'], persona['edad']]
				table.append(a)
			t = tabulate(table, headers, tablefmt="plain")
			print(t)
			archivo.write(t)
			archivo.close()
		except :
			print("Error!")

	def __init__(self):
		self.nombre = ""
		self.edad = 0
		self.arrayAmigo = []
		self.amigo()

if __name__ == "__main__":
	Amigos()