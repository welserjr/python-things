class Amigos:
	def Amigo(self):
		while True:
			dict_amigo = {}
			amigo = input("[A]migo o [S]alir: ").lower()
			if amigo == 'a':
				self.nombre = input("Nombre: ")
				self.edad = int(input("Edad: "))
				dict_amigo['nombre'] = self.nombre
				dict_amigo['edad'] = self.edad
				self.arrayAmigo.append(dict_amigo)
			elif amigo == 's':
				self.guardarAmigo(self.arrayAmigo)
				print("Adios!!!")
				break
			else:
				return self.Amigo()

	def guardarAmigo(self, amigo):
		try:
			archivo = open("amigos.txt", 'w')
			title = "---------- Lista de Amigos -----------"
			title2 = """\nNro\t Nombre \tEdad"""
			archivo.write(title)
			archivo.write(title2)
			for id, persona in enumerate(amigo):
				archivo.write("\n{}\t {} \t{}".format( id +1, persona['nombre'], persona['edad']))
			archivo.close()
		except :
			print("Error!")

	def __init__(self):
		self.nombre = ""
		self.edad = 0
		self.arrayAmigo = []
		self.Amigo()

if __name__ == "__main__":
	Amigos()