EtoF = {
	'bread': 'pan',
	'vine': 'vino',
	'eats': 'come',
	'drinks': 'toma',
	'likes': 'les gusta',
	'6.00': '6.00',
	'Everyone' : 'A todos'
}

def translateWord(word, dictionary):
	if word in dictionary:
		return dictionary[word]
	else:
		return word

def translate(sentence):
	translation = ""
	word = ""
	for c in sentence:
		if c!= ' ':
			word = word + c
		else:
			translation = translation + ' ' + translateWord(word, EtoF)
			word = ''
	return translation[1:] + ' ' + translateWord(word, EtoF)

print(translate('John eats bread'))
print(translate('Eric drinks vine'))
print(translate('Everyone likes 6.00'))