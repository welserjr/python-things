the_list = ["a", 2, 3, 1, False, [1, 2, 3]]

# Your code goes below here
one_p = the_list.pop(3)
the_list.insert(0, one_p)
del the_list[5]
del the_list[4]
del the_list[1]

print(the_list)
the_list.extend(range(4, 21))
print(the_list)
