from random import randint

class Division:
    def __init__(self):
        self.next = 'S'
        self.contDefault = 0
        self.pc = 0
        self.pi = 0
        self.juego()

    def __str__(self):
        print("Juego de Divisiones")

    def juego(self):
        print(">>> Juego de Divisiones <<<")
        while self.next == 'S':
            self.ln = self.generarRandom()
            self.x = self.ln['divisor']
            self.y = self.ln['dividendo']
            self.z = self.x / self.y
            self.rpta = True
            self.cont = 0
            self.w = 0
            while self.rpta:
                print('{} ÷ {} = ¿?'.format(self.x , self.y))
                self.w = self.validarNro()
                if self.w != 0:
                    if self.w == self.z:
                        self.contDefault += 1
                        self.pc += 1
                        print('Respuesta Correcta - {}'.format(self.w))
                        if self.contDefault == 3:
                            self.cond = True
                            while self.cond:
                                self.next = input("Seguir (S/N): ").upper()
                                if self.next == 'S' or self.next == 'N':
                                    self.contDefault = 2
                                    self.cond = False
                        self.rpta = False
                    else:
                        self.cont += 1
                        self.pi += 1
                        print('Respuesta Incorrecta - Intento {}'.format(self.cont))
                        if self.cont == 3:
                            print('Practica!!!')
                            self.cont = 0
                            self.next = 'N'
                            break
                            
            if self.next == 'N':
                print("---> Lograste <---\n" + 
                      "Rpta Correcta : {} \n".format(self.pc) +
                      "Rpta Incorrecta: {}".format(self.pi))
                print('Bye Bye :)')
                break

    def generarRandom(self):
        may = randint(1, 100)
        men = randint(1, 50)
        resto = may % men
        l = {}
        if(may < men):
            return self.generarRandom()
        else:
            if resto != 0:
                return self.generarRandom()
        l['divisor'] = may
        l['dividendo'] = men
        return l

    def validarNro(self):
        nro = input('Rpta. ->  ')
        if nro.isnumeric():
            return int(nro)
        elif nro.isspace():
            print("Ingrese un NÙMERO")
            return self.validarNro()
        else:
            print("Ingrese un NÙMERO no letra!!!")
            return self.validarNro()

if __name__ == "__main__":
    Division()                   
