import random

print("Encuentre el número!!!")

num_random = random.randint(1,10)
num_ingresados = []
intentos_permitidos = 5

while len(num_ingresados) < intentos_permitidos :

	num = input("Ingrese número entre 1 y 10: ")

	try:
		num_jugador = int(num)
	except:
		print("Número no permitido - PERDISTE!!!")
		break

	if not num_jugador > 0 or not num_jugador < 11:
		print("El número no está entre 1 y 10 - PERDISTE!!!")
		break
	
	num_ingresados.append(num_jugador)

	if num_jugador == num_random:
		print("Ganaste!!! - Mi número fue {}".format(num_random))
		print("Te tomó {} intentos".format(len(num_ingresados)))
		break
	else:
		if num_random > num_jugador:
			print("Estas cerca! Mi número es mayor que {}. Intento #{}".format(num_jugador, len(num_ingresados)))
		else:
			print("Estas cerca! Mi número es menor que {}. Intento #{}".format(num_jugador, len(num_ingresados)))
		continue

if not num_random in num_ingresados:
	print("Lo siento!! Mi numero fue {}".format(num_random))


