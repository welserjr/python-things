shopping_list = []

def show_help():
	print("\nSeparate each item with a comma.")
	print("Type DONE to quit, SHOW to see the current list, REMOVE to delete an item, "
		  "CLEAR to remote everything from the list, MOVE to move item, " 
		  "REPOS to move item by item, and HELP to get this message")

#Add item to the list.
def add_list(new_list):
	for item in new_list:
		shopping_list.append(''.join(item.strip()).capitalize())

#Add item to the list in a determinate position.
def add_list_pos(index):
	spot = int(index) - 1
	for item in new_list:
		shopping_list.insert(spot, ''.join(item.strip()).capitalize())
		spot += 1

#Display list.
def show_list():
	count = 1
	for item in shopping_list:
		print("{}: {}".format(count, item))
		count += 1

#Move item to determinate position.
def move_item(number_move, position):
	pos_item = number_move - 1
	move_to = position - 1
	item = shopping_list.pop(pos_item)
	shopping_list.insert(move_to, item)

#Move item by item.
def reposition_number(number_move, position):
	index = number_move - 1
	index_to = position - 1
	if number_move > position:
		ma_item = shopping_list.pop(index)
		me_item = shopping_list.pop(index_to)
		shopping_list.insert(index_to, ma_item)
		shopping_list.insert(index, me_item)
	else:
		ma_item = shopping_list.pop(index_to)
		me_item = shopping_list.pop(index)
		shopping_list.insert(index, ma_item)
		shopping_list.insert(index_to, me_item)

#Remove item by position.
def remove_item(idx): 
	index = idx -1
	item = shopping_list.pop(index)
	print("Remove {}.".format(item))

#Delete list.
def delete_list():
	index = len(shopping_list) - 1
	i = 0
	while index >= 0:
		del shopping_list[index]
		index -= 1
	print("List Empty")

print("Give me a list of things you want to shop for.")
show_help()

while True:
	new_stuff = input("> ")

	if new_stuff == "DONE":
		print("\nHere's your list:")
		show_list()
		break

	elif new_stuff == "HELP":
		show_help()
		continue

	elif new_stuff == "SHOW":
		show_list()
		continue

	elif new_stuff == "MOVE":
		show_list()
		number_move = int(input("Enter number to move: "))
		position = int(input("Enter Position: "))
		move_item(number_move, position)
		show_list()
		continue

	elif new_stuff == "REPOS":
		show_list()
		number_move = int(input("Enter number to move: "))
		position = int(input("Enter Position: "))
		reposition_number(number_move, position)
		show_list()
		continue

	elif new_stuff == "REMOVE":
		show_list()
		idx = input("Which item? Tell me the number. ")
		remove_item(int(idx))
		continue

	elif new_stuff == "CLEAR":
		delete_list()
		continue	

	else:
		new_list = new_stuff.split(",")
		index = input("Add this at a certain spot? Press enter for the end of the list, "
			 		  "or give me a number, Currently {} items in the list".format(len(shopping_list)))
		if index:
			add_list_pos(index)
		else:
			add_list(new_list)
