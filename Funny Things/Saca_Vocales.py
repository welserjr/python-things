distrito_names = ["Vitarte", "Surco", "Chosica", "Magdalena"]
vocales = list('aeiou')
output = []

for distrito in distrito_names:
	distrito_list = list(distrito.lower())

	for vocal in vocales:
		while True:
			try:
				distrito_list.remove(vocal)
			except:
				break

	output.append(''.join(distrito_list).capitalize())

print(output)