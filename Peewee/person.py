__author__ = 'welserjr'

from peewee import *

db = MySQLDatabase('people',  port=3306, user="", passwd="")


class Person(Model):
    name = CharField()
    birthday = DateField()
    is_relative = BooleanField()

    class Meta:
        database = db


class Pet(Model):
    owner = ForeignKeyField(Person, related_name='pets')
    name = CharField()
    animal_type = CharField()

    class Meta:
        database = db


def initialize():
    db.connect()
    db.create_tables([Person, Pet], safe=True)
    print("Databases created Successfully!")

if __name__ == "__main__":
    initialize()