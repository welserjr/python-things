__author__ = 'welserjr'

from peewee import *

db = SqliteDatabase('people.db')


class Person(Model):
    name = CharField()
    birthday = DateField()
    is_relative = BooleanField()

    class Meta:
        database = db


class Pet(Model):
    owner = ForeignKeyField(Person, related_name='pets')
    name = CharField()
    animal_type = CharField()

    class Meta:
        database = db


if __name__ == '__main__':
    db.connect()
    db.create_tables([Person, Pet], safe=True)

    from datetime import date

    # uncle_bob = Person(name='Bob', birthday=date(1960, 1, 15), is_relative=True)
    # uncle_bob.save()

    # grandma = Person.create(name='Grandma', birthday=date(1935, 3, 1), is_relative=True)
    # herb = Person.create(name='Herb', birthday=date(1950, 5, 5), is_relative=False)

    # grandma.name = 'Grandma L.'
    # grandma.save()

    # uncle_bob = Person.get(Person.name == 'Bob')
    # or uncle_bob = Person.select().where(Person.name == 'Grandma L.').get()
    # bob_kitty = Pet.create(owner=uncle_bob, name='Kitty', animal_type='cat')

    # herb = Person.get(Person.name == 'Herb')
    # herb_fido = Pet.create(owner=herb, name='Fido', animal_type='dog')

    # herb_mitens = Pet.create(owner=herb, name='Mittens', animal_type='cat')
    # herb_mittens_jr = Pet.create(owner=herb, name='Mittens Jr', animal_type='cat')

    # herb_mittens = Pet.get(Pet.name == 'Mittens')
    # herb_mittens.delete_instance()

    # herb_fido = Pet.get(Pet.name == 'Fido')
    # uncle_bob = Person.get(Person.name == 'Bob')
    # herb_fido.owner = uncle_bob
    # herb_fido.save()
    # bob_fido = herb_fido

    # for person in Person.select():
        # print(person.name, person.is_relative)

    # query = Pet.select().where(Pet.animal_type == 'cat')
    # for pet in query:
        # print(pet.name, pet.owner.name)

    # query = (Pet.select().join(Person).where(Pet.animal_type == 'cat'))
    # for pet in query:
        # print(pet.name, pet.owner.name)

    # for pet in Pet.select().join(Person).where(Person.name == 'Bob'):
        # print(pet.name)

    # uncle_bob = Person.get(Person.name == 'Bob')
    # for pet in Pet.select().where(Pet.owner == uncle_bob):
        # print(pet.name)

    # for person in Person.select().order_by(Person.birthday.desc()):
        # print(person.name, person.birthday)

    # for person in Person.select():
        # print(person.name, person.pets.count(), 'pets')
        # for pet in person.pets:
            # print('   ', pet.name, pet.animal_type)

    # subquery = Pet.select(fn.COUNT(Pet.id)).where(Pet.owner == Person.id)
    # query = (Person.select(Person, Pet, subquery.alias('pet_count')).join(Pet, JOIN.LEFT_OUTER).order_by(Person.name))
    # for person in query.aggregate_rows():
        # print(person.name, person.pet_count, 'pets')
        # for pet in person.pets:
            # print('    ', pet.name, pet.animal_type)

    # d1940 = date(1940, 1, 1)
    # d1960 = date(1960, 1, 1)
    # query = (Person.select().where((Person.birthday < d1940) | (Person.birthday > d1960)))
    # query = (Person.select().where((Person.birthday > d1940) & (Person.birthday < d1960)))
    # for person in query:
        # print(person.name, person.birthday)

    expression = (fn.Lower(fn.Substr(Person.name, 1, 1)) == 'g')
    for person in Person.select().where(expression):
        print(person.name)

    db.close()




