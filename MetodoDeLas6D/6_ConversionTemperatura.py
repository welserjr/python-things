""" Convertir la temperatura de grados Celsius a grados Fahrenheit, de acuerdo
    a la fórmula: F=(9/5)*C+32
"""

gCelsius = float(input('Ingrese la temperatura en grados Celsius: '))

gFar = (9/5) * gCelsius + 32

print('La temperatura en grados Fahrenheit es:',gFar)
