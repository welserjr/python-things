"""
    Determinar la suma de sueldos de los obreros(categoria 1), la suma de
    sueldos de los empleados(categoria 2) y la suma de los gerentes(categoria 3)
    de una determinada empresa. Adicionalmente, determinar cuál es el mayor y el
    menor sueldo en la empresa.
"""

repite = True
menorSueldo = 0
mayorSueldo = 0
sueldo1 = 0
sueldo2 = 0
sueldo3 = 0
i = 1

while repite :
    categoria = int(input("Ingrese la categoria[1,2,3]: "))
    if(categoria > 0 and categoria < 4):
        sueldo = float(input("Ingrese el sueldo: "))   
        if(i == 1):
            i = 0
            mayorSueldo = sueldo
            menorSueldo = sueldo
        else:
            if(sueldo < menorSueldo):
                menorSueldo = sueldo

            if(sueldo > mayorSueldo):
                mayorSueldo = sueldo

        if(categoria == 1):
            sueldo1 = sueldo1 + sueldo
        elif(categoria == 2):
            sueldo2 = sueldo2 + sueldo
        else:
            sueldo3 = sueldo3 + sueldo
    else:
        repite = False

print("Suma de sueldos de obreros:", sueldo1)
print("Suma de sueldos de empleados:", sueldo2)
print("Suma de sueldos de gerentes:", sueldo3)
print("El sueldo más bajo es:", menorSueldo)
print("El sueldo más alto es:", mayorSueldo)
