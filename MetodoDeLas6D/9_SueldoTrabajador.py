""" Calcular y mostrar el sueldo de un trabajador conociendo el número de horas
    trabajadas y su tarifa horaria, sabiendo que se debe descontar un 10% del
    sueldo por concepto de impuestos si este es mayor de S/.3000 .
"""

horas = int(input('Ingrese el número de horas trabajadas: '))
tarifa = float(input('Inrese la tarifa horaria: '))

sueldo = horas * tarifa

if (sueldo > 3000):
    sueldo = sueldo * 0.9

print ('El sueldo del trabajador es:',sueldo)
