"""
    Leer el sueldo y la categoria de un trabajador. Calcular el aumento
    correspondiente teniendo en cuenta lo siguiente:
    1 -> 25%
    2 -> 15%
    3 -> 10%
    4 -> 05%
    Mostrar el nuevo suelto del trabajador
"""
sueldo = float(input('Ingrese el sueldo: '))
categoria = int(input('Ingrese la categoria[1-4]: '))

if(categoria == 1 or categoria==2 or categoria==3 or categoria==4):
    if(categoria == 1):
        nuevoSueldo = sueldo * 1.25
    elif(categoria == 2):
        nuevoSueldo = sueldo * 1.15
    elif(categoria == 3):
        nuevoSueldo = sueldo * 1.10
    else:
        nuevoSueldo = sueldo * 1.05

    print('El nuevo sueldo es:',nuevoSueldo,'soles')
else:
    print('La categoria ingresada es incorrecta!!!')
