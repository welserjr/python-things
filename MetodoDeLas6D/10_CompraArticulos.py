"""
    Calcular y mostrar el monto total  pagar por al compra de varios artículos
    si se tiene que agregar el IGV(Impuesto General a las Ventas). En caso que el
    monto total a pagar incluido IGV sea mayor a S/.500 nuevos soles se aplica un
    descuento de 8%, caso contrario el descuento será de 2%
"""
montoTotal = float(input('Ingrese el sueldo: '))

montoPago = montoTotal * 1.18

if(montoPago > 500):
    montoPago = montoPago * 0.92

else:
    montoPago = montoPago * 0.98

print('El monto total a pagar incluído IGB y descuento es:',montoPago,'soles')
