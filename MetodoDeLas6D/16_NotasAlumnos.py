"""
    Calcular la nota final de todos los alumnos del curso utilizando la siguiente
    fórmula:
                 nf = (n1 + n2 + n3 + (n4 * 2)) / 5
"""

numeroAlumnos = int(input('Ingrese el número de alumnos: '))

if(numeroAlumnos > 0):
    i = 1
    while(i <= numeroAlumnos):
        nota1 = float(input("Ingrese nota1 del alumno("+str(i)+"): "))
        nota2 = float(input("Ingrese nota2 del alumno("+str(i)+"): "))
        nota3 = float(input("Ingrese nota3 del alumno("+str(i)+"): "))
        nota4 = float(input("Ingrese nota4 del alumno("+str(i)+"): "))

        nf = (nota1 + nota2 + nota3 + (nota4 * 2))/5

        print("Nota final del alumno("+str(i)+"):",nf)

        i = i +1
else:
    print('Número de alumnos inválido!!!')
