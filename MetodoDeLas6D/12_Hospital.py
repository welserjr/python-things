"""
    Calcular y mostrar el costo total que representa un paciente para un hospital
    de acuerdo al tipo de enfermedad, según los datos de la tabla. Adicionalmente
    deberá considerarse que las mujeres tienen un incremento del 1'% para el
    primer tipo de enfermedad y los hombres tienen un aumento del 5% para el
    tercer tipo de enfermedad.
    Enfermedad Tipo     Costo
                  1 -> 200
                  2 -> 350
                  3 -> 420
"""

tipoEnfermedad = int(input('Ingrese el tipo de enfermedad[1-3]: '))

if(tipoEnfermedad == 1 or tipoEnfermedad == 2 or tipoEnfermedad == 3):
    dias = int(input('Ingrese el número de días: '))
    if(dias > 0):
        if(sexo == 1 or sexo == 2):
            sexo = int(input('Ingrese su sexo (1=Masculino, 2=Femenino): '))
            if(tipoEnfermedad == 1):
                costoTotal = 200 * dias
                if(sexo == 2):
                    costoTotal = costoTotal * 1.10
            elif(tipoEnfermedad == 2):
                costoTotal = 350 * dias
            elif(tipoEnfermedad == 3):
                costoTotal = 420 * dias
                if(sexo == 1):
                    costoTotal = costoTotal * 1.05
            print('El costo total es:',costoTotal)
        else:
            print('Sexo ingresado no válido!!!')
    else:
        print('Día ingresado no válido!!!')
else:
    print('Tipo de Enfermedad no válido!!!')
