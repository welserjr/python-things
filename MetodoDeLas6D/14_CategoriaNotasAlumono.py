"""
    Un curso se evalua de la siguiente forma: se toma cinco prácticas calificadas,
    se determina el promedio de las cuatro notas más altas y se le da al estudiante
    una categoria que puede ser A, B, C o D. Según la siguiente tabla:

        Rango     Categoria
    Desde 17 a 20 --> A
    Desde 14 a 16 --> B
    Desde 10 a 13 --> C
    Desde 00 a 09 --> D
"""

nota1 = float(input('Ingrese la nota1: '))
nota2 = float(input('Ingrese la nota2: '))
nota3 = float(input('Ingrese la nota3: '))
nota4 = float(input('Ingrese la nota4: '))
nota5 = float(input('Ingrese la nota5: '))

sumaNotas = nota1 + nota2 + nota3 + nota4 + nota5

notaMenor = nota1

if(notaMenor > nota2):
    notaMenor = nota2

if(notaMenor > nota3):
    notaMenor = nota3

if(notaMenor > nota4):
    notaMenor = nota4

if(notaMenor > nota5):
    notaMenor = nota5

promedio = (sumaNotas - notaMenor)/4

if(promedio >= 17 and promedio <= 20):
    categoria = 'A'
elif(promedio >= 14 and promedio <= 16):
    categoria = 'B'
elif(promedio >= 10 and promedio <= 13):
    categoria = 'C'
else:
    categoria = 'D'

print('El promedio del alummno es',promedio,'y la categoría del alumno es"',categoria,'"')
