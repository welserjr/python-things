"""
    En una oficina de empleados, categorizan a los postulantes en función del
    sexo y de la edad, de acuerdo a lo siguiente:
     -Si la persona es de sexo femenino:
       -Categoria FA si tienen menos de 23 años
       -Categoria FB en caso contrario
    -Si la persona es de sexo maculino:
       -Categoria MA si tiene menos de 25 años
       -Categoria MB en caso contrario
"""

sexo = input('Ingrese el sexo del postulante(F/M): ')
edad = int(input('Ingrese la edad del postulante: '))

if(sexo == 'f' or sexo == 'F'):
    if(edad < 23):
        categoria = "FA"
    else:
        categoria = "FB"
elif(sexo == 'm' or sexo == 'M'):
    if(edad < 25):
        categoria = "MA"
    else:
        categoria = "MB"
else:
    print("Sexo inválido!!!")

print('La categoria del postulante es:',categoria)
