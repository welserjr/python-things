""" Ingresar el sueldo de un obrero y calcular su respectivo descuento
    del 15% por concepto de impuestos y el 5% por conceptos de cafeteria.
    Se deberá visualizar por pantalla el total de descuentos y el sueldo
    a cobraar
"""

sueldo = float(input('Ingrese el sueldo: '))

desc = sueldo * 0.2

pago = sueldo - desc

print('El total del descuento es:',desc,'soles'
      '\nEl sueldo a pagar es:',pago,'soles')
