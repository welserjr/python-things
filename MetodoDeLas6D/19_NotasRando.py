"""
    Calcular y mostrar el número de alumnos que tienen promedio final menor que 10,
    los alumnos que tienen promedio final entre 10 y 14, los alumnos que tienen
    promedio final entre 15 y 18, y los alumnos que tienen promedio final mayor que
    18. Ingresar laa 4 notas de cada alumno y calcular su promedio final en base a la
    siguiente fórmula:
                        nf = (n1 + n2 + n3 + (n4 * 2))/5
"""

total1 = 0
total2 = 0
total3 = 0
total4 = 0
i = 1
repite = True
numAlumnos = int(input("Ingrese el número de Alumnos: "))

while(i <= numAlumnos):
    while(repite):
        n1 = float(input("Ingrese nota1 del alumno("+str(i)+"): "))
        if(n1 < 0 or n1 > 20):
            print("Nota no válida!!!")
            repite = True
        else:
            repite = False
            
    repite = True
    while(repite):
        n2 = float(input("Ingrese nota2 del alumno("+str(i)+"): "))
        if(n2 < 0 or n2 > 20):
            print("Nota no válida!!!")
            repite = True
        else:
            repite = False
            
    repite = True
    while(repite):
        n3 = float(input("Ingrese nota3 del alumno("+str(i)+"): "))
        if(n3 < 0 or n3 > 20):
            print("Nota no válida!!!")
            repite = True
        else:
            repite = False
            
    repite = True
    while(repite):
        n4 = float(input("Ingrese nota4 del alumno("+str(i)+"): "))
        if(n4 < 0 or n4 > 20):
            print("Nota no válida!!!")
            repite = True
        else:
            repite = False

    nf = (n1 + n2 + n3 + (n4 * 2))/5
    print("Promedio alumno("+str(i)+"):", nf)
    
    if(nf < 10.5):
        total1 = total1 + 1

    if(nf >= 10.5 and nf < 15.5):
        total2 = total2 + 1

    if(nf >= 15.5 and nf < 17.5):
        total3 = total3 + 1

    if(nf >= 17.5):
        total4 = total4 + 1

    i = i + 1

print("Total alumnos con promedio menor a 10:", total1)
print("Total alumnos con promedio entre 10 y 14:", total2)
print("Total alumnos con promedio entre 15 y 18:", total3)
print("Total alumnos con promedio mayor a 18:", total4)
